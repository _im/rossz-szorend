String.prototype.capitalize = () ->
  this.charAt(0).toUpperCase() + this.slice(1)

vers = """
Léclépcső szélház. Celofánszekrény gőzasztal.
Gázkés légpohár. Üreskönyök, párahomlok.
Tréfája láva, mosolya iszap, füstöl.

Még cigarettázik. Harmatcsokor előtte, illata
Celofán. Kimegy: akarata vas. Szava arany.
Mosakodáshoz vizet használ, s az mindig középen van.

Lélegzete rög. A rög: buborék. A szétpukkanás: rög.
Eszmélet hajtja - eszméje: rög. Törölközője
A földre csúszik. Nem nyúl érte. NEM NYÚL ÉRTE.

Távolodó földrész. Egy nappal megint kevesebb.
Egy hanyagsággal újra több. S valakinek ez is ízlik.
Összeszedi utolsó erejét. Utolsó és üres. És összeszedi.
Homlokával elüti az időt. Ereje rög és zene.
Léptei: bánya. Az elhagyott ház sugárban okádja
A távolodást. Az út elnyeli a bútorokat. Az út: fény.

*

Papír lég pehely könnyű kisváltó váltó
közép félnehéz nehéz ólom

*

Pillangóban erős
"""

rawwords = "Léclépcső,szélház,Celofánszekrény,gőzasztal,Gázkés,légpohár,Üreskönyök,párahomlok,Tréfája,láva,mosolya,iszap,füstöl,Még,cigarettázik,Harmatcsokor,előtte,illata,Celofán,Kimegy,akarata,vas,Szava,arany,Mosakodáshoz,vizet,használ,s,az,mindig,középen,van,Lélegzete,rög,A,rög,buborék,A,szétpukkanás,rög,Eszmélet,hajtja,eszméje,rög,Törölközője,A,földre,csúszik,Nem,nyúl,érte,NEM,NYÚL,ÉRTE,Távolodó,földrész,Egy,nappal,megint,kevesebb,Egy,hanyagsággal,újra,több,S,valakinek,ez,is,ízlik,Összeszedi,utolsó,erejét,Utolsó,és,üres,És,összeszedi,Homlokával,elüti,az,időt,Ereje,rög,és,zene,Léptei,bánya,Az,elhagyott,ház,sugárban,okádja,A,távolodást,Az,út,elnyeli,a,bútorokat,Az,út,fény,Papír,lég,pehely,könnyű,kisváltó,váltó,közép,félnehéz,nehéz,ólom,Pillangóban,erős"

punctuation = "A a. A a.#A a. A, a.#A a, a a, a.##A a. A a, a#A. A: a a. A a.#A a a, a a a a a.##A a. A a: a. A a: a.#A a - a: a. A#A a a. A a a. C C C:##A a. A a a a.##A a a a. A a a a a.# A a a. A a a. A a.#A a a a. A a a a.#A: a. A a a a a#A a. A a a a a. A a: a.##*#A a a a a a#a a a a##*#A a"

words = rawwords.split(",").map((w) ->w.toLowerCase())#.sort(() -> Math.random() > 0.5)

written = []

currentpos = 0

randomCycler = 0

elementFromPunctuation = (letter, nth) ->
  options = 
    switch letter
      when "a"
        id : "word" + nth
        class : "word"
        html : words[nth]
        css :
          "text-transform" : "lowercase"
      when "A"
        id : "word" + nth
        class : "word"
        html : words[nth].capitalize()
        css :
          "text-transform" : "capitalize"
      when "C"
        id : "word" + nth
        class : "word"
        html : words[nth].toUpperCase()
        css :
          "text-transform" : "uppercase"
          
      when "#"
        html : "<br>"
        class : "punc"

      when "*"
        html : "*"
        class : "star punc"
      else
        class : "punc"
        html : letter
  $("<span>", options)  


buildskeleton = (ws, ps) ->
  nth = 0
  for i in [0..ps.length-1] 
    $("#vers").append(elementFromPunctuation(ps[i], nth))
    if ps[i] in ["a", "A", "C"]
      nth++

randomize_words = () ->
  #randomize
  # words = words.sort(() -> Math.random() > 0.5)

  #run through??
  # switched = words[randomCycler]
  # now = words[0]
  # words[0] = switched
  # words[randomCycler] = now

  #shift
  words.unshift words[words.length-1]
  words.pop()

  for i in [0..words.length - 1]
    $("#word#{i + currentpos}").html words[i]

  randomCycler = (randomCycler + 1) % words.length
  setTimeout randomize_words, 100 + currentpos * 2

writeWord = () ->
  written.push words[0]
  words.splice 0, 1
  $(".current").addClass("finished").removeClass "current"
  currentpos++
  $("#word#{currentpos}").addClass "current"

userChoice = (e) ->
  e.preventDefault()
  if words.length > 1
    writeWord()
  else
    writeWord()
    $("#choices").animate {opacity : 1.0}, 1000
  
saveAndReload = () ->
  # stringify written
  # save to localstorage or server
  window.location.reload()

init = () ->
  buildskeleton words, punctuation
  $("#rossz").click () -> window.location.reload()
  $("#rendben").click saveAndReload

  $("#word0").addClass "current"
  $("html").scrollTop 0
  $("html").keydown userChoice
  $("html").mousedown userChoice
  $("html").on "contextmenu", ((e) -> e.preventDefault())
  setTimeout randomize_words, 100

init()
